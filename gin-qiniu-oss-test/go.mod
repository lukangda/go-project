module qiniu-test

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/qiniu/go-sdk/v7 v7.11.1
)
