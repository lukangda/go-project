module rabbitmq-nginx-log

go 1.16

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/streadway/amqp v1.0.0
)
