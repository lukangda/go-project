package main

import (
	// app "im-demo/application"
	"im-demo/application/api"
	"im-demo/application/service"
	"im-demo/util/jwt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {

	gin.SetMode("debug") // debug、release、test
	engine := gin.Default()

	// 提供指定目录的静态文件支持
	engine.Static("/assets", "./assets")

	// 加载模板文件
	engine.LoadHTMLGlob("application/view/**/*")

	// 普通路由
	engine.GET("/register", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "/user/register.html", nil)
	})
	engine.POST("/register", api.Register)

	engine.GET("/login", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "/user/login.html", nil)
	})
	engine.POST("/login", api.Login)

	engine.GET("/home", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "/home/index.html", nil)
	})

	engine.GET("/search", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "/home/search.html", nil)
	})

	// websocket
	manager := service.NewManager()
	go manager.Run()
	engine.GET("/ws", func(ctx *gin.Context) {
		api.WsHandler(ctx, manager)
	})

	// 分组路由
	infoGroup := engine.Group("/info")
	infoGroup.Use(jwt.Auth())
	{
		infoGroup.GET("/userinfo", api.GetUserInfo) // 获取用户信息

		infoGroup.GET("/getoffmsg", api.GetOfflineMsg) // 获取离线消息

		infoGroup.GET("/search", api.SearchUser) // 搜索朋友

		infoGroup.GET("/contacts", api.GetContacts) // 获取好友列表

		infoGroup.POST("/friendrequest", api.FriendRequest)        // 发起好友申请
		infoGroup.GET("/friendrequest", api.GetOwnerFriendRequest) // 获取好友申请列表
		infoGroup.PUT("friendrequest", api.AgreeFriendRequest)     // 同意好友申请

		infoGroup.GET("/session", api.GetSessionList) // 获取会话列表
		infoGroup.POST("/session", api.CreateSession) // create
		infoGroup.DELETE("/session", api.DelSession)
		infoGroup.PUT("/session", api.UpdateSession) // update
	}

	// 开始监听
	_ = engine.Run(":8080")
}
