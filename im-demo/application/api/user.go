package api

import (
	"fmt"
	"im-demo/application/model"
	"net/http"
	"time"

	myjwt "im-demo/util/jwt"

	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// GetUserInfo 获取用户信息
func GetUserInfo(context *gin.Context) {
	ownerId := context.Query("owner_id")
	user, err := model.GetUserInfo(ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10201,
			"msg":  "get userinfo fail," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "get userinfo success",
		"data": user,
	})
}

// Register 注册
func Register(context *gin.Context) {
	var register model.RegisterForm

	if err := context.Bind(&register); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10100,
			"msg":  "register fail," + err.Error(),
		})
		return
	}

	if _, err := model.Register(&register); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10101,
			"msg":  "register fail," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "register success",
	})
}

// Login 登录
func Login(context *gin.Context) {
	var user model.LoginForm

	if err := context.Bind(&user); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10200,
			"msg":  "login fails," + err.Error(),
		})
		return
	}

	userinfo, err := model.Login(user)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10201,
			"msg":  "login fail," + err.Error(),
		})
		return
	}

	token, err := newToken(user)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10202,
			"msg":  "login fail," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "login successful!",
		"data": map[string]interface{}{
			"userinfo": userinfo,
			"token":    token,
		},
	})

}

// SearchUser 搜索用户，用于添加朋友
func SearchUser(context *gin.Context) {
	ownerId := context.Query("owner_id")
	search := context.Query("search")

	userList, err := model.SearchUser(ownerId, search)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10203,
			"msg":  "Fail to search user," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "search user success,",
		"data": userList,
	})
}

// FriendRequest 发送好友请求
func FriendRequest(context *gin.Context) {
	var friendRequest model.FriendForm

	if err := context.Bind(&friendRequest); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10204,
			"msg":  "Fail to send friend request," + err.Error(),
		})
		return
	}

	if err := model.CreateFriendRequest(friendRequest); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10205,
			"msg":  "Fail to send friend request," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "search user success,",
	})

}

// GetOwnerFriendRequest 获取好友请求列表
func GetOwnerFriendRequest(context *gin.Context) {
	ownerId := context.Query("owner_id")
	userList, err := model.GetOwnerFriendRequest(ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10206,
			"msg":  "Fail to get owner friend request," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "get owner friend request success",
		"data": userList,
	})
}

// AgreeFriendRequest 同意好友请求
func AgreeFriendRequest(context *gin.Context) {
	ownerId := context.Request.FormValue("owner_id")
	otherId := context.Request.FormValue("other_id")

	fmt.Println(ownerId, otherId)

	err := model.AgreeFriendRequest(ownerId, otherId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10207,
			"msg":  "Fail to agree friend request," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "agree friend request success",
	})
}

// GetContacts 获取联系人列表
func GetContacts(context *gin.Context) {
	ownerId := context.Query("owner_id")
	userList, err := model.GetContacts(ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10208,
			"msg":  "Fail to get contacts," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "get contacts succes",
		"data": userList,
	})
}

// 内部使用 ////////////////////////////////////

func newToken(user model.LoginForm) (string, error) {
	jwt := myjwt.NewJWT()
	claims := myjwt.CustomClaims{
		Name: user.Username,
		StandardClaims: jwtgo.StandardClaims{
			NotBefore: int64(time.Now().Unix() - 1000),  // Effective time of signature
			ExpiresAt: int64(time.Now().Unix() + 36000), // Expiration time is one hour
			Issuer:    "footmark",                       // Signed issuer
		},
	}
	token, err := jwt.CreateToken(claims)
	return token, err
}
