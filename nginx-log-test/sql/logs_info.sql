/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-03-25 19:01:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for logs_info
-- ----------------------------
DROP TABLE IF EXISTS `logs_info`;
CREATE TABLE `logs_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(24) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `method` varchar(24) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `protocol` varchar(24) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` int(4) NOT NULL DEFAULT '0',
  `size` int(11) NOT NULL DEFAULT '0',
  `referer` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_agent` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='nginx logs';

-- ----------------------------
-- Records of logs_info
-- ----------------------------
