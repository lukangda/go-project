/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-03-25 19:01:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for logs_info_exception
-- ----------------------------
DROP TABLE IF EXISTS `logs_info_exception`;
CREATE TABLE `logs_info_exception` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `other` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of logs_info_exception
-- ----------------------------
